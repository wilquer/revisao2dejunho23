#!/bin/bash

#EUSQ receba vários nomes de arquivo como argumento de linha comando e imprima a soma do número de linhas

soma=0
for A in $@; do
	T=$(wc -l < $A)
	(( soma += T))
done
