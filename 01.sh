#!/bin/bash

#1 - Escreva um script que, para cada arquivo dos diretórios /etc e /tmp informe se é um diretório, um arquivo, um link simbólico ou um executável.

for i in $(ls /etc/tmp); do
	[ -d $i ] && echo $i eh um diretorio && continue
	[ -f $i ] && echo $i eh um arquivo
	[ -x $i ] && echo $i eita, ainda eh executavel
	[ -L $i ] && echo $i eh link!
done

